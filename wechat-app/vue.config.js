const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  publicPath:'./',  //根域名链接
  filenameHashing:true, //命名hash处理
  productionSourceMap:false,
  // crossorigin:'use-credentials'
  devServer:{
    proxy:'http://localhost:8080'
  }
})

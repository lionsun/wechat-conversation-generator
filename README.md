<h1 align="center">微信对话生成器v1.0.1</h1>

<h4 align="center">微信对话生成器，是一款聊天记录制作工具，可以模拟微信聊天，可以添加不同用户角色进行对话，支持发送文字、语音、图片、红包、转账等。可以将聊天内容一键生成为图片和视频。</h4>
<h4 align="center">抖音上常见的聊天记录视频，使用这款工具可以轻松制作出来</h4>

<h3 align="center">
    产品界面
</h3>

<img src="./images/下载.png" width="300"/>
<img src="./images/夜间模式.png" width="300"/>

<h3 align="left">
    外观设置
</h3>

<img src="./images/外观设置.png"/>

<h3 align="left">
    对话设置
</h3>

<img src="./images/对话设置.png"/>